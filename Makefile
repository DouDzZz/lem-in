# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/03/04 18:02:25 by edjubert          #+#    #+#              #
#    Updated: 2019/05/29 17:36:19 by edjubert         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME			:=		lem-in
VISU			:=		visu

#==============================================================================#
#------------------------------------------------------------------------------#
#                                  DIRECTORIES                                 #

SRC_DIR			:=		./srcs
SRC_DIR_VISU	:=		./visualizer
INC_DIR			:=		./includes
INC_LIB			:=		./libft/includes
OBJ_DIR			:=		./obj
OBJ_DIR_VISU	:=		./obj
LIBFT_FOLDER	:=		./libft
LIBFT			:=		$(LIBFT_FOLDER)/libft.a

#==============================================================================#
#------------------------------------------------------------------------------#
#                                     FILES                                    #

SRC				:=		ants.c					\
						bfs.c					\
						cells_tools.c			\
						check_tools.c			\
						link_tools.c			\
						clean_tools.c			\
						edmonds_karp.c			\
						lem_in.c				\
						matrix.c				\
						matrix_tools.c			\
						mulpath_tools.c			\
						path_tools.c			\
						parse_tools.c			\
						super_tools.c			\
						print_tools.c			\
						visu_tools.c			\
						visu.c					\
						select_path.c

OBJ				:=		$(addprefix $(OBJ_DIR)/,$(SRC:.c=.o))
NB				:=		$(words $(SRC))
INDEX			:=		0

#==============================================================================#
#------------------------------------------------------------------------------#
#                                     FILES                                    #

SRC_VISU		:=		visualizer.c			\
						gnl_chariot_return.c	\
						visualizer_tools.c

OBJ_VISU		:=		$(addprefix $(OBJ_DIR_VISU)/,$(SRC_VISU:.c=.o))
NB_VISU			:=		$(words $(SRC_VISU))
INDEX_VISU		:=		0

#==============================================================================#
#------------------------------------------------------------------------------#
#                               COMPILER & FLAGS                               #

GCC				:=		gcc
FLAGS			:=		-Wall					\
						-Wextra					\
						-Werror					\
						-g

#==============================================================================#
#------------------------------------------------------------------------------#
#                                     RULES                                    #

all:					$(NAME) $(VISU)

$(NAME):				$(OBJ)
	@ if [ ! -f $(LIBFT) ]; then make -C $(LIBFT_FOLDER) --no-print-directory; fi
	@ $(GCC) $(FLAGS) $(OBJ) $(LIBFT) -o $(NAME)
	@ printf '\033[32m[ 100%% ] %-15s\033[92m%-30s\033[32m%s\n\033[0m' "Compilation of " $(NAME) " is done ---"

$(VISU):				$(OBJ_VISU)
	@ if [ ! -f $(LIBFT) ]; then make -C $(LIBFT_FOLDER) --no-print-directory; fi
	@ $(GCC) $(FLAGS) $(OBJ_VISU) $(LIBFT) -o $(VISU)
	@ printf '\033[32m[ 100%% ] %-15s\033[92m%-30s\033[32m%s\n\033[0m' "Compilation of " $(VISU) " is done ---"

$(OBJ_DIR_VISU)/%.o:	$(SRC_DIR_VISU)/%.c
	@ mkdir -p $(OBJ_DIR_VISU)
	@ $(eval DONE = $(shell echo $$(($(INDEX_VISU) * 20 / $(NB_VISU)))))
	@ $(eval PERCENT = $(shell echo $$(($(INDEX_VISU) * 100 / $(NB_VISU)))))
	@ $(eval TO_DO = $(shell echo "$@"))
	@ $(GCC) $(FLAGS) -I$(INC_LIB) -I$(INC_DIR) -c $< -o $@
	@ printf "                                                       \r"
	@ printf "\033[33m[ %3d%% ] %s\t%s\r\033[0m" $(PERCENT) $(VISU) $@
	@ $(eval INDEX = $(shell echo $$(($(INDEX_VISU) + 1))))

$(OBJ_DIR)/%.o:			$(SRC_DIR)/%.c
	@ mkdir -p $(OBJ_DIR)
	@ $(eval DONE = $(shell echo $$(($(INDEX) * 20 / $(NB)))))
	@ $(eval PERCENT = $(shell echo $$(($(INDEX) * 100 / $(NB)))))
	@ $(eval TO_DO = $(shell echo "$@"))
	@ $(GCC) $(FLAGS) -I$(INC_LIB) -I$(INC_DIR) -c $< -o $@
	@ printf "                                                       \r"
	@ printf "\033[33m[ %3d%% ] %s\t%s\r\033[0m" $(PERCENT) $(NAME) $@
	@ $(eval INDEX = $(shell echo $$(($(INDEX) + 1))))

clean:
	@ /bin/rm -rf $(OBJ_DIR) $(OBJ_DIR_VISU)
	@ make -C $(LIBFT_FOLDER) clean --no-print-directory
	@ printf '\033[91m[ KILL ] %-15s\033[31m%-30s\033[91m%s\n\033[0m' "CLEAN  of " $(NAME) " is done ---"
	@ printf '\033[91m[ KILL ] %-15s\033[31m%-30s\033[91m%s\n\033[0m' "CLEAN  of " $(VISU) " is done ---"

fclean:				clean
	@ /bin/rm -rf $(NAME) $(VISU)
	@ make -C $(LIBFT_FOLDER) fclean --no-print-directory
	@ printf '\033[91m[ KILL ] %-15s\033[31m%-30s\033[91m%s\n\033[0m' "FCLEAN of " $(NAME) " is done ---"
	@ printf '\033[91m[ KILL ] %-15s\033[31m%-30s\033[91m%s\n\033[0m' "FCLEAN of " $(VISU) " is done ---"

re:				fclean all

.PHONY: all clean fclean re
