/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cells_tools.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/24 10:57:32 by edjubert          #+#    #+#             */
/*   Updated: 2019/05/28 10:03:43 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

t_cell		*initialize_cell(int index, char *name)
{
	t_cell	*new;

	if (!(new = (t_cell*)malloc(sizeof(t_cell))))
		return (NULL);
	new->index = index;
	new->visited = 0;
	new->name = ft_strdup(name);
	new->next = NULL;
	return (new);
}

void		push_back_cell(t_cell **cells, int index, char *name)
{
	t_cell	*new;
	t_cell	*tmp;

	if (!cells || !*cells)
	{
		*cells = initialize_cell(index, name);
		return ;
	}
	tmp = *cells;
	while (tmp && tmp->next)
		tmp = tmp->next;
	new = initialize_cell(index, name);
	tmp->next = new;
}

int			cell_length(t_cell *cells)
{
	int		i;
	t_cell	*tmp;

	i = 0;
	tmp = cells;
	while (tmp)
	{
		i++;
		tmp = tmp->next;
	}
	return (i);
}

t_cell		*get_cell(t_cell **cells, int index)
{
	t_cell	*tmp;

	tmp = *cells;
	while (tmp && tmp->index != index)
		tmp = tmp->next;
	return (tmp);
}

t_path		*copy_path(t_path **src)
{
	t_path	*new;
	t_path	*tmp;

	tmp = *src;
	new = NULL;
	while (tmp)
	{
		push_back_path(&new, tmp->cell);
		tmp = tmp->next;
	}
	return (new);
}
