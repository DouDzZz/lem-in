/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_tools.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/13 17:44:47 by fldoucet          #+#    #+#             */
/*   Updated: 2019/05/29 18:43:08 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

void	ft_check_coordinate(char *str)
{
	int		i;
	int		space;

	i = 0;
	space = 0;
	while (str[i] && str[i] != '\n')
	{
		if (str[i] == ' ')
			space++;
		if ((str[i] < 48 || str[i] > 57) && space > 0 && str[i] != ' ')
			ft_error(4);
		i++;
	}
	if (space != 0 && space != 2)
		ft_error(4);
}

void	ft_check_map(char *str)
{
	int		i;
	int		line;
	int		se;

	i = 0;
	line = 0;
	se = 0;
	while (i < (int)ft_strlen(str)
		&& i > -1 && str[i] && !(i > 0 && str[i - 1] == '\n' && str[i] == '\n'))
	{
		if (str[i] == '\n')
		{
			line++;
			if (str[i + 1] != '#')
				ft_check_coordinate(&str[i + 1]);
		}
		if ((str[i] < 48 || str[i] > 57) && line == 0)
			ft_error(1);
		if (!ft_strncmp(&str[i], "\n##start\n", 9)
			|| !ft_strncmp(&str[i], "\n##end\n", 7))
			se++;
		i++;
	}
	if (se != 2)
		ft_error(2);
}

void	check_cells(t_cell **cells)
{
	t_cell	*tmp;
	t_cell	*tmp2;
	int		se;

	se = 0;
	tmp = *cells;
	while (tmp)
	{
		tmp2 = tmp->next;
		while (tmp2)
		{
			if (!ft_strcmp(tmp2->name, tmp->name))
				ft_error(3);
			tmp2 = tmp2->next;
		}
		if (tmp->index == 1 || tmp->index == 0)
			se++;
		tmp = tmp->next;
	}
	if (se != 2)
		ft_error(2);
}
