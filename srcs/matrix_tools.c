/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   matrix_tools.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/15 12:27:12 by edjubert          #+#    #+#             */
/*   Updated: 2019/05/24 18:38:16 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

t_matrix	*initialize_matrices(t_cell **cells, int ***map, int *len)
{
	int			i;
	int			j;
	t_matrix	*new;

	if (!(new = (t_matrix*)malloc(sizeof(t_matrix))))
		return (NULL);
	*len = cell_length(*cells);
	init_path_array(&new->path, *len, -1);
	i = -1;
	while (++i < *len)
	{
		j = -1;
		while (++j < *len)
			if ((*map)[i][j] == 1)
				(*map)[j][i] = 1;
	}
	new->map = *map;
	new->residual = create_matrice(*len);
	new->len = *len;
	return (new);
}

int			**create_matrice(int len)
{
	int		i;
	int		j;
	int		**matrice;

	if (!(matrice = (int**)malloc(sizeof(int*) * (len))))
		return (NULL);
	i = 0;
	while (i < len)
	{
		if (!(matrice[i] = (int*)malloc(sizeof(int) * len)))
			return (NULL);
		j = 0;
		while (j < len)
		{
			matrice[i][j] = 0;
			j++;
		}
		i++;
	}
	return (matrice);
}
