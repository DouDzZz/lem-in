/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   link_tools.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/23 16:30:45 by fldoucet          #+#    #+#             */
/*   Updated: 2019/05/29 18:38:47 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

void	ft_get_index(char *str, t_cell **cells, int *u, int *v)
{
	t_cell *tmp;

	tmp = *cells;
	*u = 0;
	*v = 0;
	while (tmp)
	{
		if (!ft_strncmp(str, tmp->name, ft_strlen(tmp->name)))
			*u = tmp->index;
		if (str
			&& ft_strlen(tmp->name) <= ft_strlen(str)
			&& ft_strlen(str) - ft_strlen(tmp->name) < ft_strlen(str)
			&& !ft_strncmp(&str[ft_strlen(str) - ft_strlen(tmp->name)],
				tmp->name, ft_strlen(tmp->name)))
			*v = tmp->index;
		tmp = tmp->next;
	}
}

void	ft_init_matrix(char **tab, t_cell **cells, int ***map, int size)
{
	int		i;
	int		u;
	int		v;

	u = 0;
	v = 0;
	i = 0;
	while (u < size)
	{
		v = 0;
		while (v < size)
		{
			(*map)[u][v] = 0;
			v++;
		}
		u++;
	}
	while (tab[i])
	{
		ft_get_index(tab[i], cells, &u, &v);
		(*map)[u][v] = 1;
		i++;
	}
}

void	clean_tab(char ***tab)
{
	int	i;

	i = -1;
	if (*tab)
	{
		while ((*tab)[++i])
			free((*tab)[i]);
		free(*tab);
	}
}

void	ft_parse_link(char *str, t_cell **cells, int ***map, int size)
{
	char	**tab;
	int		i;

	i = 0;
	tab = ft_strsplit(str, '\n');
	if (!((*map) = malloc(sizeof(int *) * (size + 1))))
		ft_error(100);
	while (i <= size)
	{
		if (!((*map)[i] = malloc(sizeof(int) * (size + 1))))
			ft_error(100);
		i++;
	}
	if (size > 0)
		ft_init_matrix(tab, cells, map, size);
	clean_tab(&tab);
}
