/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   matrix.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/24 17:49:58 by edjubert          #+#    #+#             */
/*   Updated: 2019/05/27 18:20:11 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

static int	check_path(t_path **save, t_matrix **matrices)
{
	t_path	*tmp;

	tmp = *save;
	while (tmp && tmp->next)
	{
		if (!(*matrices)->map[tmp->cell->index][tmp->next->cell->index])
			return (0);
		tmp = tmp->next;
	}
	return (1);
}

static int	already_in_path(t_mulpath **paths, t_path **path)
{
	t_mulpath	*tmp;

	tmp = *paths;
	while (tmp)
	{
		if ((*path)->cell->index == tmp->path->cell->index)
			return (1);
		tmp = tmp->next;
	}
	return (0);
}

void		init_path_array(int **path, int len, int value)
{
	int	i;

	if (!((*path) = (int*)malloc(sizeof(int) * len)))
		return ;
	i = -1;
	while (++i < len)
		(*path)[i] = value;
}

void		get_path_from_matrix(t_all all, int start, int limits[4])
{
	int	i;
	int	**residual;

	residual = (*all.matrices)->residual;
	i = -1;
	while (++i < (*all.matrices)->len)
	{
		if (residual[start][i] > 0)
		{
			push_back_path(all.path, get_cell(all.cells, i));
			if (!already_in_path(all.paths, all.path))
			{
				if (i == 1 && check_path(all.path, all.matrices))
				{
					push_back_mulpath(all.paths, copy_path(all.path));
					clean_paths(all.path);
					return ;
				}
				get_path_from_matrix(all, i, limits);
			}
		}
	}
}
