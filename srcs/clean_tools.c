/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   clean_tools.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/16 11:09:16 by edjubert          #+#    #+#             */
/*   Updated: 2019/05/28 10:45:09 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

void	clean_cells(t_cell **cells)
{
	t_cell	*tmp;

	while (*cells)
	{
		(*cells)->index = 0;
		ft_strdel(&(*cells)->name);
		tmp = *cells;
		*cells = (*cells)->next;
		free(tmp);
	}
}

void	clean_paths(t_path **paths)
{
	t_path	*tmp;

	while (*paths)
	{
		tmp = *paths;
		*paths = (*paths)->next;
		free(tmp);
	}
}

void	clean_mulpath(t_mulpath **paths)
{
	t_mulpath	*tmp;

	while (*paths)
	{
		tmp = *paths;
		*paths = (*paths)->next;
		clean_paths(&tmp->path);
		free(tmp);
	}
}

void	clean_matrix(t_matrix **matrices)
{
	int	i;

	i = -1;
	while (++i < (*matrices)->len)
	{
		free((*matrices)->residual[i]);
	}
	free((*matrices)->path);
	free((*matrices)->residual);
	free(*matrices);
}

void	ft_stomp(t_ant **ant)
{
	t_ant *tmp;
	t_ant *prev;

	tmp = *ant;
	prev = NULL;
	while (tmp)
	{
		if (tmp->path)
		{
			prev = tmp;
			tmp = tmp->next;
		}
		else
		{
			if (!tmp->next && prev)
				prev->next = NULL;
			else if (!prev)
				*ant = (*ant)->next;
			else
				prev->next = tmp->next;
			free(tmp);
			tmp = *ant;
		}
	}
}
