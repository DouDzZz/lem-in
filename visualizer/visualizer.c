/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   visualizer.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/14 14:42:31 by edjubert          #+#    #+#             */
/*   Updated: 2019/05/29 14:30:08 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "visualizer.h"

void	write_start(char **start, char **temp, int *started)
{
	int	to_space;

	to_space = 0;
	while ((*temp)[to_space] && (*temp)[to_space] != ' ')
		to_space++;
	*start = ft_strldup(*temp, to_space);
	*started = 0;
}

int		pull_ants(char **str, char **start)
{
	char	*temp;
	int		started;
	long	ret;

	ret = -1;
	started = 0;
	while (gnl_chariot_return(0, &temp) > 0)
	{
		if (ret == -1)
		{
			ret = ft_atol(temp);
			if (ret == 0 || ret > 2147483647 || ret < -2147483648)
			{
				ft_strdel(&temp);
				return (-1);
			}
		}
		started == 1 ? write_start(start, &temp, &started) : 0;
		started = !ft_strcmp(temp, "##start\n") ? 1 : started;
		if (temp[0] == 'L')
			*str = ft_strjoin_free(*str, temp, 0);
		else
			ft_strdel(&temp);
	}
	return (ret);
}

void	print_ant(char **ants, int fd, char *a, int k)
{
	int	nb;

	nb = ft_atoi(&a[k]);
	ft_printf("%w\t\t\t{\"id\": %d, \"source\": \"t%s\", ",
		fd, nb, ants[nb - 1]);
	if (ants[nb - 1])
	{
		free(ants[nb - 1]);
		ants[nb - 1] = ft_strdup(&a[ft_intlen(nb) + 2]);
	}
	k += ft_intlen(nb) + 1;
	ft_printf("%w\"target\": \"t%s\"", fd, &a[k]);
}

void	write_json(char *str, char **ants, int fd)
{
	int		i;
	int		j;
	int		k;
	char	**turn;
	char	**a;

	turn = ft_strsplit(str, '\n');
	i = -1;
	while (turn && turn[++i])
	{
		a = ft_strsplit(turn[i], ' ');
		j = -1;
		ft_printf("%w\t\t[\n", fd);
		while (a[++j])
		{
			k = 1;
			a[j][k] ? print_ant(ants, fd, a[j], k) : 0;
			a[j + 1] ? ft_printf("%w},\n", fd) : ft_printf("%w}\n", fd);
			ft_strdel(&a[j]);
		}
		turn[i + 1] ? ft_printf("%w\t\t],\n", fd) : ft_printf("%w\t\t]\n", fd);
		free(a);
		ft_strdel(&turn[i]);
	}
	free(turn);
}

int		main(void)
{
	int		fd;
	int		ant_nb;
	char	*str;
	char	*start;
	char	**ants;

	str = NULL;
	ant_nb = pull_ants(&str, &start);
	if (ant_nb == -1)
		return (-1);
	fd = open_file("./visualizer/ants.json");
	initialize_ants(&ants, ant_nb, start);
	open_and_close_json(fd, 1);
	write_json(str, ants, fd);
	open_and_close_json(fd, 0);
	ft_strdel(&str);
	ft_strdel(&start);
	clean_ants(&ants, ant_nb);
	return (0);
}
