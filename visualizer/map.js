/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map.js                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/10 11:45:01 by edjubert          #+#    #+#             */
/*   Updated: 2019/05/28 10:16:12 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

export class Map {
	constructor(map_path = "map.json") {
		this.svg = d3.select("svg");
		this.paused = false;
		this.high = true;
		this.radial = false;
		this.map_path = map_path;
		var width = this.svg.attr("width");
		var height = this.svg.attr("height");

		this.color = d3.scaleOrdinal().domain([1, 10])
  		.range(["red", "blue", "gold", "grey", "cyan", "grey", "darkgreen", "pink", "brown", "slateblue", "grey1", "orange"])

		this.simulation = d3.forceSimulation()
			.force("link", d3.forceLink().id((d) => d.id))
			.force("charge_force", d3.forceManyBody().strength(-6))
			.force("center_force", d3.forceCenter(width / 2, height / 2))
			.force("collide", d3.forceCollide().radius(10));

		d3.json(map_path, this.createMap.bind(this));
	}

	createMap(error, graph) {
		if (error) throw error;

		var zoom_handler = d3.zoom()
			.on("zoom", zoom_actions.bind(this));

		zoom_handler(this.svg);

		d3.select("input[type=range]")
			.on("input", inputted.bind(this));

		var tooltip = d3.select("body")
			.append("div")
			.attr("class", "tooltip")
			.style("opacity", 0);
		var link = this.svg.append("g")
			.attr("class", "links")
			.selectAll("line")
			.data(graph.links)
			.enter().append("line")
			.attr("stroke-width", (d) => Math.sqrt(d.value))
			.attr("id", d => d.source+d.target)
			.on('mouseover.tooltip', function(d) {
				tooltip.transition()
					.duration(300)
					.style("opacity", .8);
				tooltip.html(`Source:<span class="nodeName">${d.source.id.slice(1)}</span><p/>Target:<span class="nodeName">${d.target.id.slice(1)}</span></p>`)
					.style("left", (d3.event.pageX) + "px")
					.style("top", (d3.event.pageY + 10)+"px");
			})
			.on('mouseout.tooltip', function() {
				tooltip.transition()
					.duration(100)
					.style('opacity', 0);
			})
			.on('mousemouve', function() {
				tooltip.style("left", (d3.event.pageX)+"px")
					.style("top", (dd3.event.pageY)+"px");
			});

		var node = this.svg.append("g")
			.attr("class", "nodes")
			.selectAll("g")
			.data(graph.nodes)
			.enter().append("g");

		var circles = node.append("circle")
			.attr("id", (d) => d.id)
			.attr("r", (d) => d.group === 0 || d.group === 1 ? 20 : 5)
			.attr("fill", (d) => this.color(d.group))
			.on('mouseover.fade', fade(0.4))
			.on('mouseout.fade', fade(1))
			.on('mouseover.tooltip', function(d) {
				tooltip.transition()
					.duration(300)
					.style("opacity", .8);
				tooltip.html(`node:<span class="nodeName">${d.id.slice(1)}</span>`)
					.style("left", (d3.event.pageX) + "px")
					.style("top", (d3.event.pageY + 10)+"px");
			})
			.on('mouseout.tooltip', function() {
				tooltip.transition()
					.duration(100)
					.style('opacity', 0);
			})
			.on('mousemouve', function() {
				tooltip.style("left", (d3.event.pageX)+"px")
					.style("top", (dd3.event.pageY)+"px");
			})
			.call(d3.drag()
				.on("start", dragstarted.bind(this))
				.on("drag", dragged.bind(this))
				.on("end", dragended.bind(this)));

		const linkedByIndex = {};
		graph.links.forEach(d => {
			linkedByIndex[`${d.source.index},${d.target.index}`] = 1;
		})
		this.simulation
			.nodes(graph.nodes)
			.on("tick", ticked.bind(this));

		this.simulation.force("link")
			.links(graph.links);

		function isConnected(a, b) {
			return linkedByIndex[`${a.index},${b.index}`] || linkedByIndex[`${b.index},${a.index}`] || a.index === b.index;
		}

		function inputted(d) {
			this.simulation.stop();
			this.paused = false;
			document.getElementById("pauseI").classList.remove("fa-play");
			document.getElementById("pauseI").classList.add("fa-pause");
			var newValue = Math.trunc(document.getElementById("strengthBar").value);
			this.simulation.force("charge_force", d3.forceManyBody().strength(Math.trunc(newValue)));
			this.simulation.force("collide", d3.forceCollide().radius(Math.trunc(newValue) * -1));
			this.simulation.alphaTarget(0.5).restart();
		}

		function dragstarted(d) {
			this.simulation.restart();
			this.paused = false;
			document.getElementById("pauseI").classList.remove("fa-play");
			document.getElementById("pauseI").classList.add("fa-pause");
			if (!d3.event.active) this.simulation.alphaTarget(0.5).restart();
			d.fx = d.x;
			d.fy = d.y;
		}

		function dragged(d) {
			d.fx = d3.event.x;
			d.fy = d3.event.y;
		}

		function dragended(d) {
			if (!d3.event.active) this.simulation.alphaTarget(0);
			d.fx = null;
			d.fy = null;
		}

		function fade(opacity) {
			return d => {
				node.style('stroke-opacity', function (o) {
					const thisOpacity = isConnected(d, o) ? 1 : opacity;
					this.setAttribute('fill-opacity', thisOpacity);
					return thisOpacity;
				})
					.style('opacity', function (o) {
						const thisOpacity = isConnected(d, o) ? 1 : opacity;
						this.setAttribute('opacity', thisOpacity);
						return thisOpacity;
					})
				link.style('stroke-opacity', o => (o.source === d || o.target === d ? 1 : opacity));
			}
		}

		function zoom_actions() {
			var x = d3.event.transform.x;
			var y = d3.event.transform.y;
			var k = d3.event.transform.k;
			k = k < 0 ? 0 : k;
			k = k > 100 ? 100 : k;
			this.svg.attr("class", "nodes")
				.attr("transform", "scale("+k+")");
		}

		function ticked() {
			link
				.attr("x1", (d) => d.source.x)
				.attr("y1", (d) => d.source.y)
				.attr("x2", (d) => d.target.x)
				.attr("y2", (d) => d.target.y);
			node
				.attr("transform", (d) => "translate(" + d.x + "," + d.y + ")");
		}

	}

	highlight() {
		this.high = !this.high;
		d3.json(this.map_path, (error, graph) => {
			if (error) throw error;

			graph.nodes.forEach(element => {
				d3.select("#" + element.id).attr("stroke-opacity", this.high ? 1 : .1);
				d3.select("#" + element.id).attr("opacity", this.high ? 1 : .1);
			});
			graph.links.forEach(element => {
				d3.select("#" + element.source + element.target).attr("stroke-width", d => this.high ? Math.sqrt(d.value) : .1);
			});
		});

		setTimeout(() => {
			d3.json("ants.json", (error, graph) => {
				if (error) throw error;
				graph.ants.forEach(element => {
					element.forEach(turn => {
						d3.select("#" + turn.source).attr("opacity", 1);
						d3.select("#" + turn.target).attr("opacity", 1);
						d3.select("#" + turn.source + turn.target).attr("stroke-width", Math.sqrt(20));
						d3.select("#" + turn.target + turn.source).attr("stroke-width", Math.sqrt(20));
					});
				});
			});
		}, 1000);
	}

	pause() {
		this.paused ? this.simulation.restart() : this.simulation.stop();
		this.paused = !this.paused;
	}

	getPauseState() {
		return this.paused;
	}

	getHighState() {
		return this.high;
	}

	radialLayout() {
		// var links = this.simulation.force("link");
		// links.forEach(link => {
		// 	d3.select(link).remove();
		// })
		// console.log(links);
		// this.simulation.alpha(1).restart();
	}

	getRadial() {
		return this.radial;
	}
}
