/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   visualizer.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/14 14:44:10 by edjubert          #+#    #+#             */
/*   Updated: 2019/05/29 13:53:28 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef VISUALIZER_H
# define VISUALIZER_H

# include "libft.h"
# include <fcntl.h>

int		gnl_chariot_return(const int fd, char **line);
int		open_file(char *str);
void	open_and_close_json(int fd, int open);
void	initialize_ants(char ***ants, int ant_nb, char *start);
void	clean_ants(char ***ants, int ant_nb);
void	clean_split(char ***split);

#endif
